module winmain;

import core.runtime;
import core.sys.windows.windows;
import std.c.stdlib;
import std.c.string;

alias extern(Windows) void function (const(char)* progname) pfnChangeGame;
alias extern(Windows) int function( const(char)* progname, int bChangeGame, pfnChangeGame func ) pfnInit;
alias extern(Windows) void function() pfnShutdown;

pfnInit Host_Main;
pfnShutdown Host_Shutdown = null;
char *szGameDir; // safe place to keep gamedir
HINSTANCE	hEngine;

void Sys_Error( const char *errorstring )
{
	MessageBoxA( null, errorstring, "Xash Error", MB_OK|MB_SETFOREGROUND|MB_ICONSTOP );
	exit( 1 );
}

void Sys_LoadEngine()
{
	if(( hEngine = LoadLibraryA( "xash.dll" )) == null )
	{
		Sys_Error( "Unable to load the xash.dll" );
	}

	if(( Host_Main = cast(pfnInit)GetProcAddress( hEngine, "Host_Main" )) == null )
	{
		Sys_Error( "xash.dll missed 'Host_Main' export" );
	}

	// this is non-fatal for us but change game will not working
	Host_Shutdown = cast(pfnShutdown)GetProcAddress( hEngine, "Host_Shutdown" );
}

void Sys_UnloadEngine()
{
	if( Host_Shutdown ) Host_Shutdown( );
	if( hEngine ) FreeLibrary( hEngine );
}

extern (Windows) void Sys_ChangeGame( const char *progname )
{
	if( !progname || !progname[0] ) Sys_Error( "Sys_ChangeGame: NULL gamedir" );
	if( Host_Shutdown == null ) Sys_Error( "Sys_ChangeGame: missed 'Host_Shutdown' export\n" );
	strncpy( szGameDir, progname, szGameDir.sizeof - 1 );

	Sys_UnloadEngine ();
	Sys_LoadEngine ();

	Host_Main( szGameDir, 1, ( Host_Shutdown != null ) ? cast(pfnChangeGame) &Sys_ChangeGame : null );
}

extern (Windows)
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    int result;

    void exceptionHandler(Throwable e)
    {
        throw e;
    }

    try
    {
        Runtime.initialize();

        result = myWinMain(hInstance, hPrevInstance, lpCmdLine, nCmdShow);

        Runtime.terminate();
    }
    catch (Throwable o)		// catch any uncaught exceptions
    {
        MessageBoxA(null, cast(char *)o.toString(), "Error", MB_OK | MB_ICONEXCLAMATION);
        result = 0;		// failed
    }

    return result;
}

int myWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    Sys_LoadEngine();

	return Host_Main( "valve", FALSE, ( Host_Shutdown != null ) ? cast(pfnChangeGame) &Sys_ChangeGame : null );
}
